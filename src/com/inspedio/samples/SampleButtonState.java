package com.inspedio.samples;

import com.inspedio.entity.InsState;
import com.inspedio.entity.ui.InsButton;
import com.inspedio.entity.primitive.InsCallback;
import com.inspedio.enums.BorderLayout;
import com.inspedio.enums.FontSize;
import com.inspedio.enums.FontStyle;
import com.inspedio.system.core.InsCanvas;
import com.inspedio.system.core.InsGlobal;

public class SampleButtonState extends InsState{

	InsButton SPRITE;
	InsButton FONT;
	InsButton ACTION;
	InsButton DRAG;
	InsButton CAMERA;
	InsButton COLLISION;
	InsButton SAVE;
	InsButton AUDIO;
	
	InsButton EXIT;
	
	public void create(){
		int midY = InsGlobal.screenHeight / 2;
		
		int width =100;
		int height =40;
		
		int _x1 = InsGlobal.screenWidth / 4;
		int _x2 = (InsGlobal.screenWidth * 3) / 4;
		
		this.SPRITE = new InsButton(_x1, midY - 90, width, height, "SPRITE", InsCanvas.COLOR_RED);
		this.FONT = new InsButton(_x1, midY - 30, width, height, "FONT", InsCanvas.COLOR_GREEN);
		this.ACTION = new InsButton(_x1, midY + 30, width, height, "ACTION", InsCanvas.COLOR_BLUE);
		this.DRAG = new InsButton(_x1, midY + 90, width, height, "DRAG-DROP", InsCanvas.COLOR_YELLOW);
		
		this.CAMERA = new InsButton(_x2, midY - 90, width, height, "CAMERA", InsCanvas.COLOR_RED);
		this.COLLISION = new InsButton(_x2, midY - 30, width, height, "COLLISION", InsCanvas.COLOR_GREEN);
		this.SAVE = new InsButton(_x2, midY + 30, width, height, "SAVE", InsCanvas.COLOR_BLUE);
		this.AUDIO = new InsButton(_x2, midY + 90, width, height, "AUDIO", InsCanvas.COLOR_YELLOW);
		
		SPRITE.setRoundedRect(width, 40, 40, 40);
		SPRITE.setBorder(InsCanvas.COLOR_BLACK, 6);
		SPRITE.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleSpriteState(), true);
			}
		});
		
		FONT.setCaption("FONT", InsCanvas.COLOR_BLACK, FontSize.MEDIUM, FontStyle.ITALIC);
		FONT.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleFontState(), true);
			}
		});
		
		ACTION.setBorder(InsCanvas.COLOR_RED, 6, BorderLayout.OUTSIDE);
		ACTION.setCaption("ACTION", InsCanvas.COLOR_WHITE, FontSize.MEDIUM, FontStyle.BOLD);
		ACTION.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleActionState(), true);
			}
		});
		
		DRAG.setBorder(InsCanvas.COLOR_BLACK, 6, BorderLayout.CENTER);
		DRAG.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleDragDropState(), true);
			}
		});
		
		CAMERA.setRoundedRect(40, 40);
		CAMERA.setBorder(InsCanvas.COLOR_BLACK, 6);
		CAMERA.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleCameraState(), true);
			}
		});
		
		COLLISION.setCaption("COLLISION", InsCanvas.COLOR_BLACK, FontSize.MEDIUM, FontStyle.ITALIC);
		COLLISION.setClickedCallback(new InsCallback() {
			public void call() {
				InsGlobal.switchState(new SampleCollisionState(), true);
			}
		});
		
		SAVE.setBorder(InsCanvas.COLOR_RED, 6, BorderLayout.OUTSIDE);
		SAVE.setCaption("SAVE", InsCanvas.COLOR_WHITE, FontSize.MEDIUM, FontStyle.BOLD);
		SAVE.setClickedCallback(new InsCallback() {
			public void call() {
				//InsGlobal.switchState(new SampleActionState(), true);
			}
		});
		
		AUDIO.setBorder(InsCanvas.COLOR_BLACK, 6, BorderLayout.CENTER);
		AUDIO.setClickedCallback(new InsCallback() {
			public void call() {
				//InsGlobal.switchState(new SampleDragDropState(), true);
			}
		});
		
		this.add(this.SPRITE);
		this.add(this.FONT);
		this.add(this.ACTION);
		this.add(this.DRAG);
		
		this.add(this.CAMERA);
		this.add(this.COLLISION);
		this.add(this.SAVE);
		this.add(this.AUDIO);
		
		InsButton EXIT = new InsButton(30, InsGlobal.screenHeight - 20, 60, 40, "EXIT", 0xFFFFFF);
		EXIT.setBorder(0, 3);
		EXIT.setCaption("EXIT", 0, FontSize.LARGE, FontStyle.BOLD);
		EXIT.setClickedCallback(new InsCallback() {
			public void call() {
				onLeftSoftKey();
			}
		});
		
		this.add(EXIT);
	}
	
	
	public void onLeftSoftKey()
	{
		InsGlobal.save.save();
		InsGlobal.exitGame();
	}
}
