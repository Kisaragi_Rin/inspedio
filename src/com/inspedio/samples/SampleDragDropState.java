package com.inspedio.samples;

import com.inspedio.entity.InsState;
import com.inspedio.entity.primitive.InsCallback;
import com.inspedio.entity.sprite.InsDragDropSprite;
import com.inspedio.entity.ui.InsButton;
import com.inspedio.enums.FontSize;
import com.inspedio.enums.FontStyle;
import com.inspedio.system.core.InsGlobal;

public class SampleDragDropState extends InsState{

	public void create() {
		final InsDragDropSprite sprite1 = new InsDragDropSprite("/com/inspedio/sample/sprite.png", InsGlobal.screenWidth / 2, InsGlobal.screenHeight / 2, 32, 48);
		
		sprite1.setDragDropCallback(new InsCallback() {
			public void call() {
				System.out.println("Drag Start");
				handleDrag(sprite1);
			}
		}, new InsCallback() {
			public void call() {
				System.out.println("Drag Finish");
				handleDrop(sprite1);
			}
		});
		
		this.add(sprite1);
		
		
		InsButton back = new InsButton(30, InsGlobal.screenHeight - 20, 60, 40, "BACK", 0xFFFFFF);
		back.setBorder(0, 3);
		back.setCaption("BACK", 0, FontSize.LARGE, FontStyle.BOLD);
		back.setClickedCallback(new InsCallback() {
			public void call() {
				onLeftSoftKey();
			}
		});
		
		this.add(back);
	}
	
	public void handleDrag(InsDragDropSprite s){
		
	}
	
	public void handleDrop(InsDragDropSprite s){
		
	}
	
	public void onLeftSoftKey()
	{
		InsGlobal.switchState(new SampleButtonState(), false);
	}

}
