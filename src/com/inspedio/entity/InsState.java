package com.inspedio.entity;

import java.util.Vector;

import com.inspedio.enums.CollisionType;
import com.inspedio.system.core.InsGlobal;
import com.inspedio.system.helper.InsKeys;
import com.inspedio.system.helper.InsPointer;
import com.inspedio.system.helper.collision.InsCollisionCallback;
import com.inspedio.system.helper.collision.InsCollisionDetector;

/**
 * InsState represents GameState, like MenuState, or PlayState.<br>
 * Its basically a container of Game Object with ability to pause, reset, and load progressively.<br>
 * Some State can be very heavy (contain many objects), therefore it needs to load progressively.<br>
 * Because of that, State Constructor should be emptied, and object initiation should be declared on <code>create()</code><br>
 * 
 * @author Hyude
 * @version 1.0
 */
public abstract class InsState extends InsGroup{

	protected InsCollisionDetector detector;
	
	/**
	 * DO NOT Override this unless you know what you are doing.
	 */
	public InsState()
	{
		super();
		this.detector = new InsCollisionDetector();
	}
		
	/**
	 * Handling Key and PointerEvent before passing it to its member
	 */
	public void update()
	{
		this.handleKeyState(InsGlobal.keys);
		this.handlePointerEvent(InsGlobal.pointer);
		super.update();
	}
	
	public void postUpdate(){
		super.postUpdate();
		this.detector.handleAllCollision();
		InsGlobal.camera.postUpdate();
	}
	
	/**
	 * This function is called after the game engine successfully switches states.
	 * Override this function, NOT the constructor, to initialize or set up your game state.
	 * We do NOT recommend overriding the constructor, unless you want some crazy unpredictable things to happen!
	 */
	public abstract void create();
	
	/**
	 * Things to do after create, and after set to Current GameState
	 */
	public void finishCreate()
	{
		InsGlobal.loadProgress = 100;
	}
	
	public void destroy()
	{
		super.destroy();
	}
	
	/**
	 * This function is called to reset game
	 * Default are destroying state and creating it
	 */
	public void reset()
	{
		this.clear();
		this.members = new Vector();
		this.create();
	}
	
	/**
	 * Pause State. Override this to implement your Pause Behavior
	 */
	public void pause()
	{
		
	}
	
	/**
	 * Resume State. Override this to implement your Resume Behavior
	 */
	public void resume()
	{
		
	}
	
	/**
	 * Override this to implement State KeyEvent handler, such as pausing game.
	 */
	public void handleKeyState(InsKeys key)
	{
		
	}
	
	/**
	 * Override this to implement State PointerEvent handler, such as pausing game.
	 */
	public void handlePointerEvent(InsPointer pointer)
	{
		for(int i = 0; i < pointer.pressed.length; i++){
			this.onPointerPressed(pointer.pressed[i].x, pointer.pressed[i].y);
		}
		
		for(int i = 0; i < pointer.released.length; i++){
			this.onPointerReleased(pointer.released[i].x, pointer.released[i].y);
		}
		
		for(int i = 0; i < pointer.dragged.length; i++){
			this.onPointerDragged(pointer.dragged[i].x, pointer.dragged[i].y);
		}
		
		for(int i = 0; i < pointer.hold.length; i++){
			this.onPointerHold(pointer.hold[i].x, pointer.hold[i].y);
		}
	}
	
	/**
	 * Action executed when LeftSoftKey Pressed
	 */
	public void onLeftSoftKey()
	{
		
	}
	
	/**
	 * Action executed when RightSoftKey Pressed
	 */
	public void onRightSoftKey()
	{
		
	}
	
	/**
	 * Add Collision Detector Event <br>.
	 * 
	 * @param	Name		Name of Event
	 * @param	Group1		List of First Object (ex : player)
	 * @param	Group2		List of Second Object (ex : bullet)
	 * @param	Tolerance	Tolerance Value. Higher tolerance means object must be closer to collide.
	 * @param	Callback	Callback of Event (what to do to both object when it collide)
	 * @param	Type		Collision Type, Either BOX or SPHERE
	 * 
	 */
	public void addCollisionEvent(String Name, InsGroup Group1, InsGroup Group2, int Tolerance, CollisionType Type, InsCollisionCallback Callback){
		this.detector.addHandler(Name, Group1, Group2, Tolerance, Type, Callback);
	}
	
	/**
	 * Remove Collision Event from State
	 */
	public void removeCollisionEvent(String Name){
		this.detector.removeHandler(Name);
	}
	
	
}
